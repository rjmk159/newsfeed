import axios from 'axios';
import swal from 'sweetalert';

let api = {};
var BASE_URL = 'https://api.rss2json.com/v1/api.json?rss_url=';


let getDefaultHeaders = () => {
    return {
      headers: {

      }
    }
  }
  
api.getDefaultHeaders = getDefaultHeaders;
  

api.get = (url) => {
    let headers = getDefaultHeaders();
    return new Promise(function (resolve, reject) {
      axios.get(`${BASE_URL}${url}`, headers)
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          api.handleError(error, null);
        })
    });
  }


  api.handleError = (error) => {
    if (error && error.response) {
        swal({
            title: "Error!",
            text: "Not a Valid URL",
            icon: "error"
          });
    } else {
      swal({
        title: "Error!",
        text: "something went wrong, please try again!",
        icon: "error"
      });
    }
  }
 
  export default api; 
    
  
  