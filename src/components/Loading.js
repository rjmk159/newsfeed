import React from 'react';

class Loading extends React.Component {
    render(){
        return (
            <div className="loader">
                <img src={require('../assets/images/loader.gif')} />
            </div>
        );
        }
    }

export default Loading;
