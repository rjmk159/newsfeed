import React from 'react';
import renderHTML from 'react-render-html';

class MainContent extends React.Component {
  render(){
    return (
      <div className="rss-feeds-html-renderer">
            <div className="feed-content">
            {renderHTML(this.props.content)}
            </div>
            <div className="feed-author">
            {this.props.author?<span><i className="fa fa-user _author"></i>{this.props.author}</span>:''}
            </div>
            <div className="feed-categories">
                <ul>
                  {this.props.categories && this.props.categories.length>0 ? this.props.categories.map((value,index)=>{
                    return( <li><i class="fa fa-tags _tags"></i> <span>{value}</span></li>)
                  }):null}
                </ul>
            </div>
      </div>
    );
  }
}

export default MainContent;
