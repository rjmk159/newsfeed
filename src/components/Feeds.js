import React from 'react';
import RssFeedsCard from './Feeds.style'
import {Helmet} from "react-helmet";
import MainContent from "./MainContent"
class Feeds extends React.Component {
constructor(props){
  super(props);
  this.state={
      showMore:false,
  }
}

render(){
  return (
    <RssFeedsCard className="rss-feeds-card">
        <Helmet>
        <meta charSet="utf-8" />
        <title>{this.props.title}</title>
        <link rel="canonical" href="  " />
        </Helmet>
        <p className="feed-owner">{this.props.title}<span>{'1 day ago'}</span></p>
        <p className="feed-title">   <a href={this.props.link} rel="noopener noreferrer" target="_blank">{this.props.heading}</a></p>
        <span className="read-more" onClick={()=>this.setState({showMore:!this.state.showMore})}>{!this.state.showMore?'Read more':'Hide more'} </span>
        {this.state.showMore ? <MainContent content = {this.props.item} featuredImage={this.props.featuredImage} />:null}   
      
    </RssFeedsCard>
  )
}
}

export default Feeds;
