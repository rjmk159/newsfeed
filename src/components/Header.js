import React from 'react';
class Header extends React.Component {
render(){
return (
  <div className="rss-feeds-header">
      {this.props.image?<img src={""} alt="#"/>:!this.props.title?<i className="fa fa-rss-square _logo-header" aria-hidden="true"></i>:<i className="fa fa-question-circle _logo-header" aria-hidden="true"></i>}
      <p>{this.props.title?this.props.title:'Feeds'}</p>
  </div>
);
}
}

export default Header;
