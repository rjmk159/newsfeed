
import React from 'react';
import 'font-awesome/css/font-awesome.min.css';
import CardFeed from './UrlsCard.style'
const enhanceWithClickOutside = require('react-click-outside');





// browserHistory.listen( location =>  {
//       console.log(location)
//    });


class UrlsCard extends React.Component {
    constructor(props){
     super(props);
     this.state = {
        showPopup:false,
     }
    }
    
    handleClickOutside() {
      this.setState({showPopup:false});
    }
    
    render(){
        const selected =  this.props.isSelected === this.props.id? true:false;
        
        return (
            <CardFeed className={`history-feeds-cards ${selected?'selected':''}`}>
              <p onClick={()=>this.props.url?this.props.onClick(this.props.url):null} id={`history-card-${this.props.id}`} className="feed-url-or-name">{this.props.image?<img src={this.props.image} alt="#"/>:this.props.url?<i className="fa fa-question-circle _logo" aria-hidden="true"></i>:null}{this.props.url}</p>
             {!this.props.isActive ? <i className="fa fa-times _delete" onClick={()=>{this.props.deleteHistory(this.props.id)}}></i>:null}
                 <i className="fa fa-ellipsis-h _moreDetails" onClick={()=>{this.setState({showPopup:!this.state.showPopup})}}></i>
                 <div className="feed-url-details-popup" style={this.state.showPopup?{opacity:1,visibility:'visible'}:{}}>
                     <ul>
                     <li><span>Feed URL:<a target="_blank" rel="noopener noreferrer" href={this.props.url}>{this.props.url}</a></span></li>
                     </ul>
                 </div>
            </CardFeed>
        );
    }
}
UrlsCard.defaultProps = {
    isActive: false,
    isSelected:false,
  };

export default enhanceWithClickOutside(UrlsCard);
