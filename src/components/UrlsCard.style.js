import styled from 'styled-components';
const CardFeed = styled.div`
display: flex;
cursor:pointer;
align-items: center;
justify-content: space-between;
margin-right: 4px;
padding: 7px 0px;
margin: 0 -10px;
padding:0 10px;
position:relative;
._logo{
    margin-right:7px;
    max-width:14px;
    width:14px;
    overflow:hidden;
    height:auto;
}
&:hover {
    background: #d3d4d6;
}
p.feed-url-or-name {
    font-size: 14px;
    font-size: 14px;
    text-overflow: ellipsis;
    max-width: 238px;
    white-space: nowrap;
    overflow: hidden;
    padding: 10px 0;
    overflow: hidden;
}
i:hover{color:red;cursor:pointer}
i.fa.fa-ellipsis-h._moreDetails {
    position: absolute;
    opacity:0;
    right: 41px;
    bottom: 4px;
    font-size: 10px;
    cursor: pointer;
    color:#5B4ECF;
}
&:hover{
    i.fa.fa-ellipsis-h._moreDetails{
        opacity:1;
    }
}
.feed-url-details-popup {
    position: absolute;
    background: #fff;
    right:5px;
    bottom: -85px;
    z-index: 1;
    border-radius: 4px;
    opacity: 0;
    visibility: hidden;
    border-color: rgba(0,0,0,0.08);
    box-shadow: 0 16px 20px rgba(0,0,0,0.2);
    width: 109px;
    padding: 10px;
    min-height: 65px;
    &:before {
        height: 16px;
        width: 16px;
        background: #fff;
        content: '';
        top: -3px;
        left: 80px;
        position: absolute;
        transform: rotate(135deg);
        z-index: -1;
    }
    a {
        word-break: break-all;
    }
}
    ul {
        list-style: none;
        padding: 0;
        margin: 0;
        font-size: 10px;
    }
}
`;

export default CardFeed;