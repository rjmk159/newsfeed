
import styled from 'styled-components';
const RssFeedsCard = styled.div`
    padding: 18px;
    box-shadow: 0 1px 0 rgba(0,0,0,0.08);
    position:relative;
    p.feed-owner {
        font-size: 13px;
        font-weight: 600;
        opacity: 0.7;
        padding-bottom: 10px;
        display: flex;
        align-items: center;
        justify-content: space-between;
  
        span{
            color: #5b4ecf;
            font-size: 11px;
        }
    }
    p.feed-title {
        font-weight: 600;
        padding-right:100px;
        a {
            text-decoration: none;
            color: #000;
        &:hover{
            text-decoration:underline;
        }
        }
    }
    span.read-more {
        background: rgba(0,0,0,.08);
        color: #707070;
        font-size: 13px;
        font-style: normal;
        font-stretch: normal;
        text-align: center;
        border-radius: 5px;
        cursor: pointer;
        white-space: nowrap;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        vertical-align: top;
        transition: background .2s,border .2s,color .2s,box-shadow .2s;
        text-decoration: none;
        font-weight: 500;
        outline: 0;
        border: none;
        height: 26px;
        line-height: 32px;
        padding: 0 9px;
        font-weight: 600;
        cursor: pointer;
        margin-top: 0;
        position: absolute;
        right: 17px;
        top: 48px;
        &:hover{
            opacity:0.5
        }    
    }
.rss-feeds-html-renderer {
    position:relative;
    font-size: 14px;
    padding: 40px 0;
    span.read-more{
        position: absolute;
            right: 0px;
            top: auto;
            bottom:25px;
    }
    img{
        max-width: 100%;
        height: auto;    
    }
}
.feed-author {
    padding-top: 50px;
    font-weight: 600;
    i._author {
        margin-right: 10px;
        font-size: 20px;
        color: black;
    }
}
.feed-categories ul {
    list-style: none;
    padding: 0;
    margin-top: 30px;
    li {
        display: inline-block;
        padding-right: 22px;
        span{
            color: #5b4ecf;
            font-weight: 600;
            opacity: 0.7;
        }

        i._tags{
            margin-right: 10px;
            font-size: 20px;
            color: black; 
            opacity:1
        }

    }
}
`;

export default RssFeedsCard;