
import styled from 'styled-components';
const RssFeedsContentContainer = styled.div`
    width: calc(100% - 300px);
    &:last-child {
      margin-bottom: 60px;
    }
    .rss-feeds-header {
      height: 50px;
      display: flex;
      align-items: center;
      justify-content: center;
      box-shadow: 0 1px 0 rgba(0,0,0,0.08);
      background: #EEEFF0;
      border-left: 1px solid #dcdcdc;
      margin-left: -1px;
  }
  ._logo-header {
    color: #000;
    margin-right: 10px;
    max-width: 15px;
    height: auto;
    width: 15px;
        box-shadow: 0 1px 0 rgba(0, 0, 0, 0.08);
  }
`;

export default RssFeedsContentContainer;