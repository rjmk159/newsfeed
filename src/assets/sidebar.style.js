import styled from 'styled-components';
const RssFeedsSidebar = styled.div`
background: #EEEFF0;
height: 100vh;
max-width: 100%;
width: 300px;
.sidebar-header{
    box-shadow: 0 1px 0 rgba(0, 0, 0, 0.08);
    height: 50px;
    font-size: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: 'Lobster', cursive;
    font-weight:500;
    position:relative;
    i._rss {
        position: absolute;
        left: 10px;
        cursor:pointer;
        color:orange;
        font-size: 25px;
        &:hover{
            opacity:0.5;
        }
    }
}//Sidebar End
.input-container {
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 10px;
    margin-top:10px;
    input{
        font-size: 13px;
        font-weight: 400;
        font-style: normal;
        font-stretch: normal;
        outline: 0;
        height: 16px;
        border: none;
        background: #d3d4d6;
        border-radius: 5px;
        padding: 8px;
        border-top-right-radius: 0;
        color: #000;
        border-bottom-right-radius: 0;
        width: calc(100% - 40px);
    }
    button{
        background: #5B4ECF;
        color: #fff;
        font-size: 13px;
        font-style: normal;
        font-stretch: normal;
        text-align: center;
        border-radius: 5px;
        cursor: pointer;
        white-space: nowrap;
        display: inline-flex;
        justify-content: center;
        align-items: center;
        vertical-align: top;
        text-decoration: none;
        font-weight: 500;
        outline: 0;
        border: none;
        height: 32px;
        line-height: 32px;
        padding: 0 16px;
        border-top-left-radius: 0;
        color: #fff;
        border-bottom-left-radius: 0;
        -webkit-appearance: none;
        border:1px solid #5B4ECF;
        &:hover{
            opacity:0.8;
        }
    }
}
.rss-feed-active-url-container {
    padding: 10px;
        ._history {
            margin-right: 4px;
        }
        ._active {
            color: #00de00;
        } 
    }
    .rss-feed-history-url-container {
        height: calc(100vh - 240px);
        overflow-x: hidden;
        padding-right: 10px;
        padding-left: 10px;
    }   
    .rss-feed-url-container ._title {
        padding: 0 10px;
    }    
    span._title {
    margin-bottom: 5px;
    display: inline-block;
}    
.history-feeds-cards.selected{
    background:#d3d4d6;
    p{
        font-weight:600;
    }
}
`;

export default RssFeedsSidebar;