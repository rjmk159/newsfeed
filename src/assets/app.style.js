
import styled from 'styled-components';
const RssFeedsContainer = styled.div`
    font-family: 'Open Sans', sans-serif;
    height:100vh;
    overflow:hidden;
    display: flex;
    
    *{
      transition:0.2s ease-in-out;
    }
    i{
      color: #949698;
    }
    p{
      margin:0;
    }
    .feeds-container {
      margin-bottom: 80px;
      height:100%;
      .no-data{
        display:flex;
        align-items:center;
        justify-content:center;
        height:100%
      }
  }
  form {
    display: flex;
    width: 100%;
}
.loader {
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
}
`;

export default RssFeedsContainer;