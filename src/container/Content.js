import React from 'react';
import RssFeedsContentContainer from '../assets/content.style'
import Header from '../components/Header';
import Feeds from '../components/Feeds';
import Loading from '../components/Loading';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';

class Content extends React.Component {

render(){
return (
  <RssFeedsContentContainer className="rss-feeds-content-container">
    <Header
    title={this.props.response && this.props.response.feed ? this.props.response.feed.url:''}
    image={this.props.response && this.props.response.feed ? this.props.response.feed.image:''}
    />
    <PerfectScrollbar>
    {!this.props.isLoading?<div className={`feeds-container ${this.props.isLoading ? 'hasLoader':'' }`}>
      
      {this.props.response && this.props.response.items.length > 0 ? this.props.response.items.map((value,index)=>{
       return (
        <Feeds 
          key={index}
          title = {this.props.response && this.props.response.feed ? this.props.response.feed.title:''}
          link = {value.link}
          item = {value.content}
          author = {value.author}
          heading = {value.title}
          date={value.pubDate}
          featuredImage={value.thumbnail}
          categories={value.categories}
        />)  
      }):<div className="no-data">No data to show...</div>}
    </div>:<Loading/>}
    </PerfectScrollbar>
  </RssFeedsContentContainer>
);
}
}

export default Content;
