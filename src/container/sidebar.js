import React from 'react';
import RssFeedsSidebar from '../assets/sidebar.style'
import 'font-awesome/css/font-awesome.min.css';
import UrlsCard from '../components/UrlsCard';
import { connect } from 'react-redux';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import {locationHistory} from '../redux/action'
import history from '../utils/history';
const _validation = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
class sidebar extends React.Component {
constructor(props){
  super(props);
  this.state={
      showSideBar:true,
      inputUrl:'',
      url:'',
  }
}
handleSubmitUrl=(e)=>{
    e.preventDefault();
    let _url = this.state.inputUrl.trim();
    if(!_url.match(_validation)){
      return;
    };
 
    this.setState({url:_url})
    this.props.handleSubmitUrl(_url,true)
}
handleClickUrl=(url,index)=>{
  console.log('this.prop.history[this.props.selected]',this.props.history[this.props.selected])
  history.push({
    pathname: '/',
    search: `?selected=${index}`
  })
  let _url = url.trim();
  this.setState({url:_url})
  this.props.handleSubmitUrl(_url,false,index);
  this.props.locationHistory(this.props.history[this.props.selected]);
}
render(){
return (
  <RssFeedsSidebar className={`rss-feeds-sidebar ${this.state.showSideBar ? 'open':'close'}`}>
    <div className="sidebar-header">
    <i className="fa fa-rss-square _rss"></i> Rss Feeds
    </div>
    <div className="rss-feed-sidebar-container">
    <div className="input-container">
      <form onSubmit={(e)=>this.handleSubmitUrl(e)}>
        <input className="url-input" type="text"  onChange={(e)=>{this.setState({inputUrl:e.target.value})}} placeholder="Feed URL…"/>
        <button className="url-button"><i className="fa fa-search _search"></i></button>
      </form>
    </div>
    <div className="rss-feed-active-url-container">
    <span className="_title"><i className="fa fa-circle _active"></i>  Active</span>
    {this.props.isActiveFeed?(
      <UrlsCard 
      isActive={true}
      url={this.state.url || this.props.history[this.props.selected]}
      onClick={(url)=>{this.handleClickUrl(url)}}

      />
    ):<p style={{fontSize:'10px'}}>No Active URL</p>}
       
    </div>
    <div className="rss-feed-url-container">
        <span className="_title"><i className="fa fa-history _history"></i>  History</span>
        <PerfectScrollbar> 
        <div className="rss-feed-history-url-container">
          {this.props.history && this.props.history.length > 0 ? this.props.history.map((value,index)=>(
            <UrlsCard
            key={index}
            id={index}
            isActive={false}
            url={value}
            onClick={()=>{this.handleClickUrl(value,index)}}
            isSelected={this.props.selected}
            deleteHistory = {(index)=>{this.props.deleteHistory(index)}}
            />
          )):<p style={{fontSize:'10px'}}>No history</p>}

        </div>
        </PerfectScrollbar>
    </div>
    </div>

  </RssFeedsSidebar>
);
}
}

const mapStateToProps = state => ({
  ...state
 
 });
 
 export default connect(mapStateToProps, {locationHistory})(sidebar);