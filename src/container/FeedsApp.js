  import React from 'react';
import RssFeedsContainer from '../assets/app.style';
import Sidebar from './sidebar';
import Content from './Content';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import {getFeedDetails,addHistory,deleteHistory} from '../redux/action'
import _ from 'lodash';
import { createBrowserHistory } from 'history';
const history = createBrowserHistory(); 
var counter=0
class FeedsApp extends React.Component {
constructor(props){
  super(props);
  this.state = {
    response:null,
    history:[],
    isActiveFeed:false
  }
}



componentDidMount(){
  this.getHistory(this.props)
  this.setState({response:this.props.currentFeedListing})
  this.setState({isActiveFeed:this.props.isActive})
}
componentWillReceiveProps(nextProps){
  if(!_.isEqual(nextProps.currentFeedListing,this.props.currentFeedListing)){
    this.setState({response:nextProps.currentFeedListing})
  }
  this.getHistory(nextProps)
  this.setState({response:this.props.currentFeedListing})
  this.setState({isActiveFeed:this.props.isActive})
}
componentDidUpdate(prevProps) {

  const unlisten = history.listen((location, action) => {
    // location is an object like window.location
    // console.log(action, location.pathname, location.state);
    
    if(counter<1){
    if(action=='POP'){
      console.log(">>>>",prevProps.locationHistory)
      this.props.getFeedDetails(prevProps.locationHistory,()=>{
        this.setState({response:prevProps.locationHistory});
    })
    counter++
    }
  }


  });
  
  
}

onRouteChanged=(id)=> { 
  if(id!==''){
  id =  id.split('=');
  let _selected = id[1];
  let url = this.props.history[_selected]
  console.log(url,id)
  this.props.getFeedDetails(url,id,()=>{
    this.setState({response:this.props.currentFeedListing});
})
  }
}

handleSubmitUrl = (url,insert,index=0) => {
  if(insert){
    this.setHistory(url);
  }
  this.props.getFeedDetails(url,index,()=>{
      this.setState({response:this.props.currentFeedListing});
  })

}



getHistory = (props)=>{
  if(props.history && props.history.length > 0){
    this.setState({history:props.history})
  }
}
setHistory = (url)=>{
  let history = this.props.history || [url];
    history.map((value,index)=>{
      if(url === value){
        history.slice(index,1);
      }
    })
  let  _history = [url, ...history];
  this.props.addHistory(_history);
}
deleteHistory = (index) =>{
  let last = false;
  console.log(index)
  let history = this.props.history || [];
  console.log(history )
  if(history.length>0){
    if(history.length==1){
      last = true;
    }
    history.splice(index,1);
 
    this.props.deleteHistory(history,last,()=>{
    this.setState({flag:true})
    console.log(this.props)
    });
  }

}
render(){
    return (
      <RssFeedsContainer className="rss-feeds-container">
        <Sidebar
          handleSubmitUrl = {(url, insert,index)=>this.handleSubmitUrl(url, insert,index)}
          response={this.state.response}
          isActiveFeed ={this.props.isActive}
          history={this.props.history}
          selected ={this.props.selected}
          getFeedDetails = {(index)=>this._getFeedDetails(index)}
          deleteHistory = {(index)=>this.deleteHistory(index)}
        />
        <Content response={this.state.response} isLoading={this.props.isLoading}/>
      </RssFeedsContainer>
    );
  }
}
const mapStateToProps = state => ({
 ...state

});

export default withRouter(connect(mapStateToProps, {getFeedDetails,addHistory,deleteHistory})(FeedsApp));