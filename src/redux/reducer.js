const initialState = {
  history: [],
  isLoading: false,
  isActive: false,
  selected: 0,
  currentFeedListing: null,
  locationHistory: []
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_FEED":
      return {
        ...state,
        currentFeedListing: action.response,
        isLoading: false,
        isActive: true,
        selected: action.index
      };
    case "SET_LOADING":
      return { ...state, isLoading: action.status };
    case "SET_HISTORY":
      return { ...state, history: action.history };
    case "LOCATION_HISTORY":
      return { ...state, locationHistory: action.locationHistory };
    case "DELETE_HISTORY":
      return {
        ...state,
        history: action.history,
        currentFeedListing: action.last ? null : state.currentFeedListing,
        isActive: action.last ? false : state.isActive
      };
    default:
      return { ...state };
  }
};

export default rootReducer;
