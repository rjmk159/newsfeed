// import { createStore, applyMiddleware  } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
// import thunk from "redux-thunk";
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web and AsyncStorage for react-native
// import rootReducer from './reducer';
import { createStore, applyMiddleware, compose  } from 'redux';
import thunk from 'redux-thunk';
 import rootReducer from './reducer';
import { BrowserRouter } from 'react-router-dom'
import { routerMiddleware } from 'react-router-redux';
const persistConfig = {
  key: 'root',
  storage,
}
 
const middleWares = [
    thunk,
    routerMiddleware(BrowserRouter)]
const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(
	persistedReducer,	
	compose(
        applyMiddleware(
            ...middleWares
        )
    )
)
const persistor = persistStore(store)


export default () => {
    return { store, persistor }
  }