
import api from '../utils/services';


export function getFeedDetails(url,index,callback=()=>{}) {
  return dispatch => {
    dispatch(setLoadiing(true))
    api.get(url)
    .then((response, error) => {
      dispatch(setFeed(response,index));
      callback()
    }).catch((error)=>{
      dispatch(setLoadiing(false));
    }) 
  }
}

export function setFeed (response,index){
  return {
    response,
    index,
    type: "SET_FEED",
  };
}
export function setLoadiing (status){
  return {
    status,
    type: "SET_LOADING",
  };
}
export function addHistory (history){

  return {
    history,
    type: "SET_HISTORY",
  };
}

export function locationHistory (locationHistory){
  return {
    locationHistory,
    type: "LOCATION_HISTORY",
  };
}

export function deleteHistory (history,last,callback=()=>{}){
    callback();
    return{
      last,
      history,
      type: "DELETE_HISTORY",
    }
 
  };

