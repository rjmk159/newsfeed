import React,{Component} from 'react'
import { Provider } from 'react-redux';
import { Router, Route, Switch } from 'react-router'
import { createBrowserHistory } from 'history'
import { PersistGate } from 'redux-persist/integration/react'
import FeedsApp from './container/FeedsApp'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import { createStore, applyMiddleware, compose  } from 'redux';
import thunk from 'redux-thunk';
 import rootReducer from './redux/reducer';
import { BrowserRouter } from 'react-router-dom'
import { routerMiddleware } from 'react-router-redux';
const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['history','isLoading','isActive','selected','currentFeedListing'],
  }
   
  const middleWares = [
      thunk,
      routerMiddleware(BrowserRouter)]
  const persistedReducer = persistReducer(persistConfig, rootReducer)
  const store = createStore(
      persistedReducer,	
      compose(
          applyMiddleware(
              ...middleWares
          )
      )
  )
  const persistor = persistStore(store)
class App extends Component {

  render(){ 
    console.log("store",store,persistor)
      return ( 
        <Provider store={store}>
              <PersistGate loading={null} persistor={persistor}>
            <Router history={createBrowserHistory()} >
                <Switch >
                    <Route exact path="/" component={FeedsApp}/>
                </Switch>
            </Router>     
            </PersistGate>                 
        </Provider>
        )
    }
}

export default (App);